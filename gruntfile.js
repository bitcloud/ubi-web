module.exports = function(grunt){
	grunt.initConfig({
		concat: {
			js:{
				src: ['js/jquery-2.1.1.js', 'js/pace.min.js', 'js/bootstrap.min.js',
				'js/classie.js', 'js/cbpAnimatedHeader.js', 'js/wow.min.js',
				'js/ubiscript.js'],
				dest: 'ubi.js'
			},
			css:{
				src: ['css/bootstrap.min.css', 'css/animate.min.css',
				'css/ubistyle.css'],
				dest: 'ubi.css'
			}
		},
		uglify: {
			dist: {
				src: 'ubi.js',
				dest: 'build/ubi.min.js'
			}
		},
		cssmin: {
		  css:{
			src: 'ubi.css',
			dest: 'build/ubi.min.css'
		  }
		},
		shell: {
			multiple: {
				command: [
					'del ubi.js',
					'del ubi.css',
					//'mkdir deploy',
					'move build\\ubi.min.js deploy\\ubi.min.js',
					'move build\\ubi.min.css deploy\\ubi.min.css'
				].join('&&')
			}
		}
	});
	
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-shell');
	
	//Default task.
	grunt.registerTask('default', ['concat', 'uglify', 'cssmin', 'shell']);
};