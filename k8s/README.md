# ubicando.pe

## Overview

*INSERT NETWORK DIAGRAM*

## Cloud architecture
This is deployed in Kubernetes Digital Ocean. The cluster components are:
- 1 worker node with `1vCPU`, `2GB RAM`, `50GB Disk`. 
- A load balancer with a public ipaddr `178.128.135.19`.

The kubernetes cluster has been deployed and configured from tohe Digital Ocean 
console, but all its components are being dynamically allocated with k8s yaml files.

## Application architecture

The application consists of 1 deployment and 1 services in the `ubicando` namespace.

### ubicando-app

Main pod holding the ubicando application.

##### Containers 

- **init**: It's an init container to check out the ubi-web code in the `ubicando-data` volume mounted in `/data` which is an emptyDir.

- **ubicando**: It uses the `nginx:latest` image running in port `80`. It mounts ubi-web files from the `ubicando-data` volume in `/usr/share/nginx/html`. 

##### Configmaps

- **ubi-config**: configMap holding the `GIT_USER`.

##### Secrets

- **ubi-secret**: secret holding the `GIT_PASSWORD`.

##### Service

- **ubicando-service**: It listens in a [clusterIp](https://kubernetes.io/docs/concepts/services-networking/service/#proxy-mode-userspace) forwarding to selector `app=ubi` in port `80` 

 
## Networking

### Ingress Controller
The cluster uses a [nginx](https://github.com/kubernetes/ingress-nginx) 
[ingress controller](https://kubernetes.io/docs/concepts/services-networking/ingress/#ingress-controllers) a
s a manager for external access serving as a reverse proxy and ssl termination.
The ingress controller runs a deployment and a service. The service generates a 
`load balancer` in Digital Ocean to open the cluster to the Internet.
It listens in port 80 and 443. The installation of the nginx ingress controller is 
specified in [this guide](https://www.digitalocean.com/community/tutorials/how-to-set-up-an-nginx-ingress-with-cert-manager-on-digitalocean-kubernetes).

    NAME                                            READY   STATUS    RESTARTS   AGE
    pod/nginx-ingress-controller-565dfd6dff-knx8j   1/1     Running   0          16d

    NAME                    TYPE           CLUSTER-IP       EXTERNAL-IP      PORT(S)                      AGE
    service/ingress-nginx   LoadBalancer   10.245.233.187   178.128.135.19   80:30973/TCP,443:30284/TCP   16d

    NAME                                       READY   UP-TO-DATE   AVAILABLE   AGE
    deployment.apps/nginx-ingress-controller   1/1     1            1           16d

    NAME                                                  DESIRED   CURRENT   READY   AGE
    replicaset.apps/nginx-ingress-controller-565dfd6dff   1         1         1       16d

### Ingress resource

As the ingress controller exposes HTTP and HTTPS routes from outside the cluster 
to services inside the cluster. An [ingress resource](https://kubernetes.io/docs/concepts/services-networking/ingress/#the-ingress-resource) 
routes traffic by defined rules.

- **ubicando-ingress**: It forwards traffic directed to `ubicando.pe` to the 
`ubicando-service` in port `80`. It also defines an annotation to increse the 
`max_body_size` to be uploaded.

### SSL

SSL termination is defined as an annotation. It uses `letsencrypt` to generate 
a TLS certificate through the [cert-manager](https://cert-manager.readthedocs.io/en/latest/index.html) service which manages the certificate 
lifecycle. Certificates can be requested and configured by annotating ingress 
resources with the `certmanager.k8s.io/issuer` annotation, appending a tls section 
to the Ingress spec, and configuring one or more [issuers](https://cert-manager.readthedocs.io/en/latest/reference/issuers.html) 
to specify your preferred certificate authority.
The installation and configuration are straightforward:

    echo "Deploying cert-manager" helm install --name cert-manager --namespace kube-system stable/cert-manager
	echo "Deploying letsencrypt issuer"
	kubectl create -f ubicando-issuer.yaml
    echo "Deploying ingress"
    kubectl apply -f ubicando-ingress.yaml

The `issuer` and the `certificate` are deployed in the `ubicando` namespace.
